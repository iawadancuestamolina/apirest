<?php

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Listar todos los productos
Route::get('/products', function () {
    return new ProductCollection(Product::all());
});

// Listar un producto por ID
Route::get('/product/{id}', function ($id) {
    return new ProductResource(Product::find($id));
});

// Crear un nuevo producto
Route::post('/products', [ProductController::class, 'store']);

// Modificiar un product
Route::patch('/products', [ProductController::class, 'update']);

// Eliminar un producto
Route::delete('/products', [ProductController::class, 'delete']);

// Crear un nuevo usuario
Route::post('/users', [RegisteredUserController::class, 'new']);

// Cambiar los datos de un usuario
Route::patch('/users', [RegisteredUserController::class, 'update']);

// Eliminar un usuario
Route::delete('/users', [RegisteredUserController::class, 'delete']);

// Modifica la función de listar todos productos para que ademas muestre el nombre y la id de la categoria en la misma petición
